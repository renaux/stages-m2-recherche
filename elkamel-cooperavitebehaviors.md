# Study of Cooperative Driver Behaviors at Adjacent Roadway

## Présentation générale

Sujet pas encore affecté.

### Résumé

To solve the problem caused by congestions in autonomous traffic system with some cooperative methods. The goal of this project is to study and implement algorithms for optimizing the movements of autonomous vehicles in adjacent traffic roadway. The objective is to compute the optimal location, speed and acceleration/deceleration of vehicles equipped with V2V communication to conduct lane change or lane keeping on the two adjacent lanes, respectively. Besides, eco-driving will be considered as a way to minimize the fuel consumption and improve sustainability of environment.

Algorithms can have better performance in collision-free ability and efficiency improvement.

The topic is applied in a combination of vehicular communication and vehicle longitudinal/lateral dynamics.

### Mots-clés

Intelligent traffic system, Cooperative algorithms, Design & simulation, Optimization & decision making  

### Encadrement

Équipe(s) : OSL- CRIStAL

Encadrant(s) :

- Abdelkader EL KAMEL (Enseignant/Chercheur à Centrale Lille/abdelkader.elkamel@centralelille.fr)
- Qing SHI (Ph.D student à Centrale Lille/qing.shi@centralelille.fr)

[Contacter les encadrants.](mailto:qing.shi@centralelille.fr)

Localisation: Centrale Lille - Bâtiment C 320b

## Présentation détaillée

### Prérequis

- Algorithms / UML
- Programming languages (C, C++, C#...)
- MATLAB/SIMULINK
- Element of optimization theory and multi-agent technology

### Contexte
Our team has put many efforts in control and optimization in Intelligent Transportation System (ITS), we have established a platoon of vehicles in the lateral and longitudinal control system, and achieved a trajectory mechanism for autonomous intersection management. This project is carried out for lane-changing management in ITS, and seeks to design an effective management mechanism for connected and automated vehicles using optimization approaches. Each vehicle using optimization approaches and multi-agent technology in this approach is described as an individual reactive agent which can communicate with other reactive agents to determine the optimal movements. The main task is to model the collaborative algorithm between vehicle agents to overcome any selfish driving behaviors, including building gap acceptance model for host agent to perform changing behavior into the adjacent lane, and an algorithm of decision making for mainline vehicles to accelerate or decelerate. In other words, vehicle agents could collaborate, for instance, they could play in a form of a “cooperative game”, and allocate the pay-off (benefit) among players’ choices (strategies) in a fair way. The object is to ensure a global benefit for all vehicles, while the global benefit is defined as minimizing the total delay and ensuring no collisions inside this behavior-changing area, and eco-driving will be considered as a way to minimize the fuel consumption and improve sustainability of environment. Therefore, this project lies at the intersection between the area of vehicular communication and vehicle longitudinal/lateral dynamics.

### Problématique
Individual vehicles equipped with features of autonomous technologies, such as inter-communication ability, cruise control, autonomous steering, which will be used in order to solve eco-driving autonomous ITS. Especially in two adjacent lanes segment, two competing traffic demands are arousing our research interest. For example, the complex and adaptive behavior between acceleration and gap acceptance, as well as the cooperative behavior on the main lane can result in conflicts and trigger congestion. In this project, we propose to study, design and implement different algorithms to generate gap-acceptance decisions during the changing process.

### Travail à effectuer

 - Study the related bibliography
 -  Train on existing tools (multi-agent simulation in MATLAB, traffic communication simulation in SUMO/Python ...)  
- Design the network of traffic scenarios  
- Implement and test via simulation the proposed algorithms



### Bibliographie

[1] Liu, Bing, and Abdelkader El Kamel. "V2x-based decentralized cooperative adaptive cruise control in the vicinity of intersections." _IEEE Transactions on Intelligent Transportation Systems_ 17.3 (2016): 644-658.

[2] Liu, Bing, Qing Shi, Zhuoyue Song, and Abdelkader El Kamel. "Trajectory planning for autonomous intersection management of connected vehicles." _Simulation Modelling Practice and Theory_ 90 (2019): 16-30.
(https://authors.elsevier.com/a/1X~8n,ZhUEM2Wv)

[3] Milanés, Vicente, et al. "Automated on-ramp merging system for congested traffic situations." _IEEE Transactions on Intelligent Transportation Systems_ 12.2 (2011): 500-508.  
(https://hal.inria.fr/file/index/docid/737629/filename/Automated_On-Ramp_Merging_System_for_Congested_Traffic_Situations.pdf)

[4] Rios-Torres, J. and Malikopoulos, A.A., 2017. A survey on the coordination of connected and automated vehicles at intersections and merging at highway on-ramps. _IEEE Transactions on Intelligent Transportation Systems_, _18_(5), pp.1066-1077.
(https://www.osti.gov/pages/servlets/purl/1328275)

[5] Al-Sultan, Saif, et al. "A comprehensive survey on vehicular ad hoc network." _Journal of network and computer applications_ 37 (2014): 380-392._(http://iranarze.ir/wp-content/uploads/2018/05/9011-English-IranArze.pdf)  

[6] Li, Li, Ding Wen, and Danya Yao. "A survey of traffic control with vehicular communications." _IEEE Transactions on Intelligent Transportation Systems_ 15.1 (2014): 425-432.  
